# Repository Tugas 3 JarKom
Anggota Kelompok :
- Inigo Ramli, NPM : 1806133742
- Andre Theodore Tjondrowidjojo, NPM : 1806133862
- Christopher Samuel, NPM : 1806141151

**For step-by-step guide on setting up your AWS environment, go to [this](docs/README.md) guide**

## Tujuan Repository:
Menyimpan source code dari arsitektur master-worker yang kami implementasikan
untuk tugas 3 Jaringan Komputer (CSCM603154) Universitas Indonesia oleh Kelompok C3.

## Cara Menjalankan sesuai Kriteria Tugas 3
Pertama, jalankan instance EC2 sebanyak jumlah master node + worker nodes
yang akan dipakai. Setelah itu, di masing-masing instance, kami melakukan
git clone repository ini dan mengaktifkan master serta worker nodes nya di
instance yang berbeda-beda. Kemudian, buka public IP dari master node
untuk mengakses portal webservernya untuk mendaftarkan worker nodes dan
membuat jobs yang akan di-assign. Petunjuk lebih lanjut dapat dilihat dalam
dokumentasi berikut:  https://docs.google.com/document/d/1JBXz1pCR4Kwix03ADR4Jm6cKzTjOZhMOjiujiMZXLbc/edit?usp=sharing.

## File-file Penting
- thread.py : membuat thread-thread yang berhubungan dengan worker nodes, seperti thread untuk mendengarkan sebuah socket
- worker/base.py : melakukan parsing Job parameters dan membuat worker node untuk mengerjakan Job yang sesuai
- Django project files umum : kami gunakan untuk membuat portal master node 

## Penjelasan Parameter Master Node Portal
Ketika aplikasi masterapp dijalankan, master node akan berjalan, dan user
bisa menggunakan website yang terhubung untuk mengaksesnya. Untuk petunjuk
menulis isi dari parameters, berikut adalah penjelasannya berdasarkan job type:
- Summation : integer1 integer2 integer3 ... integerN
- Given N. Count how many possibility of a * b = c * d : a b c d
- Levenshtein's Distance : string1 string2
- Exponentiation : a[0] a[1] a[2] ... a[n]
Jenis input dari semua job types kecuali Levenshtein's Distance adalah kumpulan integer yang
dipisahkan whitespace, sementara untuk Levenshtein's Distance, inputnya adalah
2 String yang dipisahkan whitespace. Worker node akan secara mem-parse parameter
tersebut dan mengerjakan job yang sesuai.
