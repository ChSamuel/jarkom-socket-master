# Configurations for Setting Up This Application

Below is the step-by step guide for setting up this application in an AWS environment:

1. Create a custom VPC, name it "tugas-3-vpc" (or other names you prefer). Set the network range to 10.0.0.1/16.
2. Create a subnet on a location you like and set it's range to 10.0.1.0/24
	- Create an internet gateway and associate it with this subnet
	- Set the route table according to the configuration below. Use the internet gateway you have created before.
	![](rt-conf.png)
	- Confirm that your "Network ACL" follows the default configuration below.
	![](acl-conf.png)
3. Create two security groups: "master-sg" and "worker-sg"
	- For the "master-sg" group, configure the inbound traffic to accept the connections below:
	![](master-sc-conf.png)
	This will allow users to perform requests to the webserver. For the SSH request, you can restrict the access to only your personal computer for better security.
	- For the "worker-sg" group, configure the inbound traffic to accept the connections below:
	![](worker-sg-conf.png)
	This will restrict the worker nodes from being triggered by the public internet, and allows communication only to the master node (and other workers). This connection scheme gives the master node another role as the "bastion" of the network. This makes securing the network easier.	
4. Create at least two Amazon Linux instance. One for master node, and the rest for workers
	- Put all of the nodes inside the subnet you have created.
	- Ensure that the master node has the "Auto Assign Public IP" enabled. Otherwise, you will need to associate an elastic IP to it. Otherwise, mimic this configuration:
	![](master-instance-conf.png)
	- Worker nodes should not have a public IP:
	![](worker-instance-conf.png)
	- Save the private key for accesing these nodes.
	- Note down the private IPs for your worker nodes.
5. Perform an SSH connection to your master node.
6. Perform the usual update with "sudo yum update", and install both Git and python3 using "sudo youm install git python3"
7. Clone this repository inside your master node.
8. Use the "scp" command to transfer your private key generated at step 4c to your master node. You will need it to further acces each of your worker nodes.
9. From your master node, perform an SSH connection to your worker node.
10. Perform the usual update with "sudo yum update", and install both Git and python3 using "sudo youm install git python3"
	- If these commands return an error, you may need to temporarily attach an elastic IP to your worker nodes.
11. Clone this repository inside your master node and cd to it's directory.
12. Detach your terminal using the command "screen". This allows you to keep processes running in the beckground.
13. Run the base.py file with the command "python3 worker/base.py"
14. When prompted, enter the designated port number. You can use 1337, or any number larger than 1024.
15. Detach the screen by pressing "Ctrl+A" and press "d".
16. Logout from your worker node back to the master node.
17. Repeat step 9-15 for each of your worker nodes.
18. In order to make your master node accessible using HTTP to the public, you need to run a webserver inside it. Install and run a webserver using these commands:
	- "sudo yum install httpd"
	- "sudo service httpd start"
	- "sudo service httpd status" (to check that the webserver is running properly)
18. "cd" in to your cloned Git repository.
19. Detach your terminal using the command "screen".
20. Install all of the Python requirements using the command "pip3 install -r requirements.txt"
21. Run the server using these command sequences:
	- "python3 manage.py makemigrations"
	- "python3 manage.py migrate"
	- "python3 manage.py runserver 0.0.0.0:8000"
22. Detach the screen by pressing "Ctrl+A" and press "d".
23. Your server is now fully set up and you can logout from the SSH connection.
24. Confirm that your new website for job scheduling is running by browsing to "http://[YOUR_MASTER_NODE_PUBLIC_IP]:8000"
25. On the "Register Worker" section of your webapp, submit the lists of your private IPs and ports you have noted down on step 4d.
26. Now your webapp is fully running, and you can start sending jobs through it.
